package com.guavapay.mailserver.service;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {
    void sendEmail(SimpleMailMessage simpleMailMessage);
}
