package com.guavapay.mailserver.rabbit;

import com.guavapay.mailserver.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RabbitConsumer {

    private final EmailService emailService;

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void receivedMessage(SimpleMailMessage simpleMailMessage) {
        emailService.sendEmail(simpleMailMessage);
    }
}
